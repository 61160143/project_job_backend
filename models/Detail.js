const mongoose = require('mongoose')
const Schema = mongoose.Schema
const detSchema = new Schema({
  name: String,
  province: String,
  recruitment: Number,
  salary: Number,
  postion: String,
  startDate: Date,
  endDate: Date,
  information: String,
  email: String,
  website: String
})

module.exports = mongoose.model('Detail', detSchema) || mongoose.models.Detail
