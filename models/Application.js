const mongoose = require('mongoose')
const Schema = mongoose.Schema
const applicationSchema = new Schema({
  jobposting: {
    type: Schema.Types.ObjectId,
    ref: 'jobPosting'
  },
  company: {
    type: Schema.Types.ObjectId,
    ref: 'Company'
  },
  info: [
    {
      _id: false,
      jobseeker: {
        type: Schema.Types.ObjectId,
        ref: 'jobSeeker'
      },
      status: String
    }
  ]
})

module.exports = mongoose.model('Application', applicationSchema) || mongoose.models.Application
