const mongoose = require('mongoose')
const Schema = mongoose.Schema
const comSchema = new Schema({
  name: String,
  userName: String,
  email: String,
  location: String,
  website: String,
  isApprove: Boolean
})

module.exports = mongoose.model('Company', comSchema) || mongoose.models.Company
