const mongoose = require('mongoose')
const Schema = mongoose.Schema
const positionSchema = new Schema({
  name: String,
  skills: {
    type: Schema.Types.ObjectId,
    ref: 'Skill'
  }
})

module.exports = mongoose.model('Position', positionSchema) || mongoose.models.Position
