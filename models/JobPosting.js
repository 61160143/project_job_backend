const mongoose = require('mongoose')
const Schema = mongoose.Schema
const jobPostingSchema = new Schema({
  position: {
    type: Schema.Types.ObjectId,
    ref: 'Position'
  },
  company: {
    type: Schema.Types.ObjectId,
    ref: 'Company'
  },
  recruitment: Number,
  startDate: Date,
  endDate: Date,
  province: String,
  salary: Number,
  description: String,
  createDate: {
    type: Date,
    default: new Date().toLocaleString('en-US', { timeZone: 'Asia/Bangkok' })
  }
})

module.exports = mongoose.model('jobPosting', jobPostingSchema) || mongoose.models.JobPosting
