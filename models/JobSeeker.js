const mongoose = require('mongoose')
const Schema = mongoose.Schema
const jobSeekerSchema = new Schema({
  name: String,
  email: String,
  phoneNumber: String,
  birthdate: Date,
  resume: {
    path: String
  },
  isApprove: {
    type: Boolean,
    default: false
  }
})

module.exports = mongoose.model('jobSeeker', jobSeekerSchema) || mongoose.models.JobSeeker
