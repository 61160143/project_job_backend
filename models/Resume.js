const mongoose = require('mongoose')
const Schema = mongoose.Schema
const ResumeSchema = new Schema({
  position: {
    type: Schema.Types.ObjectId,
    ref: 'Position'
  },
  company: {
    type: Schema.Types.ObjectId,
    ref: 'Company'
  },
  email: String,
  ApplicantName: String
})
module.exports = mongoose.model('Resume', ResumeSchema) || mongoose.models.Resume
