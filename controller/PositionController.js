const Position = require('../models/Position')
const positionController = {
  async addPosition (req, res, next) {
    const payload = req.body
    // res.json(userController.addUser(payload))
    const position = new Position(payload)
    try {
      await position.save(position)
      res.json(position)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updatePosition (req, res, next) {
    const payload = req.body
    // res.json(userController.updateUser(payload))
    try {
      const position = await Position.updateOne({ _id: payload._id }, payload)
      res.json(position)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deletePosition (req, res, next) {
    const { id } = req.params
    // res.json(userController.deleteUser(id))
    try {
      const position = await Position.deleteOne({ _id: id })
      res.json(position)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getPositions (req, res, next) {
    try {
      const positions = await Position.find({})
      res.json(positions)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async getPosition (req, res, next) {
    const { id } = req.params
    try {
      const positions = await Position.findById(id)
      res.json(positions)
    } catch (err) {
      res.status(500).send(err)
    }
  }

}

module.exports = positionController
