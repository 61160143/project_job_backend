const JobPosting = require('../models/JobPosting')
const Position = require('../models/Position')
const jobPostingsController = {
  async addJobPosting (req, res, next) {
    const payload = req.body

    const getPosition = await Position.find({ name: payload.position.name })
    if (getPosition.length === 0) {
      const create = new Position({
        name: payload.position.name
      })
      create.save()
      payload.position = create._id
    } else {
      payload.position = getPosition[0]._id
    }

    const jobPosting = new JobPosting(payload)
    try {
      await jobPosting.save()
      res.json(jobPosting)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateJobPosting (req, res, next) {
    const payload = req.body
    // res.json(usersController.updateUser(payload))
    try {
      const getPosition = await Position.find({ name: payload.position.name })
      if (getPosition.length === 0) {
        const create = new Position({
          name: payload.position.name
        })
        create.save()
        payload.position = create._id
      } else {
        payload.position = getPosition[0]._id
      }
      const jobPosting = await JobPosting.updateOne(
        { _id: payload._id },
        payload
      )
      res.json(jobPosting)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async delJobPosting (req, res, next) {
    const { id } = req.params
    // res.json(usersController.deleteUser(id))
    try {
      const jobPosting = await JobPosting.deleteOne({ _id: id })
      res.json(jobPosting)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getJobPostings (req, res, next) {
    try {
      const jobPosting = await JobPosting.find({})
        .sort({ createDate: -1 })
        .populate('position')
        .populate('company')
        .exec()
      res.json(jobPosting)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getJobPosting (req, res, next) {
    const { id } = req.params
    try {
      const jobPosting = await JobPosting.findById(id)
        .populate('position')
        .populate('company')
        .exec()
      res.json(jobPosting)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getJobPostingByCompany (req, res, next) {
    const { id } = req.params
    try {
      const jobPosting = await JobPosting.find({
        company: id
      })
        .populate('position')
        .populate('company')
        .exec()
      res.json(jobPosting)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getSearchJobPostings (req, res, next) {
    const keepIdPosition = []
    const { keyword, province } = req.body
    try {
      await Position.find({
        name: { $regex: keyword, $options: 'i' }
      }).then(data => {
        data.forEach(item => {
          keepIdPosition.push(item._id)
        })
      })
      
      const jobPosting = await JobPosting.find({
        $and: [{ province: province }, { position: { $in: keepIdPosition } }]
      })
        .populate('position')
        .populate('company')
      res.json(jobPosting)
      
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = jobPostingsController
