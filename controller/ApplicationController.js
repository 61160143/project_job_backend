const Application = require('../models/Application')
const JobSeeker = require('../models/JobSeeker')
const applicationsController = {
  async getApplication (req, res, next){
    const { id } = req.params
    try {
      const application = await Application.find({ company: id })
        .populate('company')
        .populate('jobposting')
        .populate('info.jobseeker')
        .exec()
      res.json(application)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async acceptApplication(req,res,next) {
    const payload = req.body
    try{
      const application = await Application.findByIdAndUpdate( payload._id, {status: 'accept'})
      await JobSeeker.findByIdAndUpdate( payload.jobseeker, { isApprove: true})
      res.json(application)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async declineApplication(req,res,next) {
    const payload = req.body
    try{
      const application = await Application.findByIdAndUpdate( payload._id, {status: 'decline'})
      await JobSeeker.findByIdAndUpdate( payload.jobseeker, { isApprove: false})
      res.json(application)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async registerApplication(req, res, next) {
    const payload = req.body
    const temp = {
      jobseeker: payload.jobseeker, status: 'wait'
    }
    try {
      const application = await Application.findOneAndUpdate( { jobposting: payload.jobposting }, 
        {
          $addToSet: { 
            info: temp
          } 
        }
      )
      console.log(application)
      res.json(application)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = applicationsController