const Resume = require('../models/Resume')
const resumeController = {
  async getResume (req, res, next) {
    try {
      const resume = await Resume.find({})
        .populate('position')
        .populate('company')
        .exec()
      res.json(resume)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = resumeController
