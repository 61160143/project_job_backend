const JobSeeker = require('../models/JobSeeker')
const jobSeekersController = {
  async addJobSeeker (req, res, next) {
    const payload = req.body
    console.log(payload)
    const jobSeeker = new JobSeeker(payload)
    try {
      await jobSeeker.save()
      res.json(jobSeeker)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateJobSeeker (req, res, next) {
    const payload = req.body
    // res.json(usersController.updateUser(payload))
    try {
      const jobSeeker = await JobSeeker.updateOne({ _id: payload._id }, payload)
      res.json(jobSeeker)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateResume (req,res,next) {
    const payload = req.body
    try{
      const jobSeeker = await JobSeeker.findByIdAndUpdate( payload._id, {resume: payload.resume})
      res.json(jobSeeker)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteJobSeeker (req, res, next) {
    const { id } = req.params
    // res.json(usersController.deleteUser(id))
    try {
      const jobSeeker = await JobSeeker.deleteOne({ _id: id })
      res.json(jobSeeker)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getJobSeekers (req, res, next) {
    try {
      const jobSeeker = await JobSeeker.find({})
      res.json(jobSeeker)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getJobSeeker (req, res, next) {
    const { id } = req.params
    try {
      const jobSeeker = await JobSeeker.findById(id)
      res.json(jobSeeker)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = jobSeekersController
