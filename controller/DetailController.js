const Detail = require('../models/Detail')
const detailController = {

  async getDetail (req, res, next) {
    const { id } = req.params
    try {
      const detail = await Detail.findById(id)
      res.json(detail)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = detailController
