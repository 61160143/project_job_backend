const ObjectID = require('mongodb').ObjectID
module.exports = [
  {
    _id: new ObjectID('602fbe6eceddfb124692ae39'),
    name: 'Clicknext',
    userName: 'Suphayod Sirijamroonwit',
    email: 'info@clicknext.com',
    location: '128/232-333 30rd Floor Phayathai Plaza Ratchathewi Bangkok 10400',
    website: 'https://www.clicknext.com',
    isApprove: true
  },
  {
    _id: new ObjectID('605953033a4a95e45640e006'),
    name: 'SymonHoiji Inc.',
    userName: 'Weerasak Seanglers',
    email: 'info@symonhoiji.com',
    location: '156/26 30rd Floor Phayathai Plaza Ratchathewi Bangkok 10400',
    website: 'https://www.symonhoiji.com',
    isApprove: true
  }
]
