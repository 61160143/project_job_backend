const ObjectID = require('mongodb').ObjectID
module.exports = [
  {
    _id: new ObjectID('6057850f8275851efc5b5083'),
    username: "test",
    email: "a@a.com",
    password: "$2a$08$AtSGZD8MzU.AOSv3p9/0PealXgPdKVU7r10ROtJ9KmEWy4hhF4RXm",
    roles: [
      new ObjectID("60577ee37598e969329c4678"), 
      new ObjectID("60577f3bec1e8d174d484d90")
    ],
    jobseeker: new ObjectID("601eb76d1e72782ca8ad6178")
  },
  {
    _id: new ObjectID('6058be820cf56870a4238729'),
    username: "test2",
    email: "a@a.com",
    password: "$2a$08$AtSGZD8MzU.AOSv3p9/0PealXgPdKVU7r10ROtJ9KmEWy4hhF4RXm",
    roles: [
      new ObjectID("60577ee37598e969329c4678"), 
      new ObjectID("60577f3894d00ab321c77799")
    ],
    company: new ObjectID("602fbe6eceddfb124692ae39")
  },
  {
    _id: new ObjectID('6058c7213bd1ff27241dc41a'),
    username: "company",
    email: "a@a.com",
    password: "$2a$08$AtSGZD8MzU.AOSv3p9/0PealXgPdKVU7r10ROtJ9KmEWy4hhF4RXm",
    roles: [
      new ObjectID("60577ee37598e969329c4678"), 
      new ObjectID("60577f3894d00ab321c77799")
    ],
    company: new ObjectID("6058c7d695bdbb026870de6e")
  }
]
