const ObjectID = require('mongodb').ObjectID
module.exports = [
  {
    _id: new ObjectID('6044c1399a01c1226801c854'),
    jobposting: new ObjectID('602fe36a92e124475c0036a8'),
    company: new ObjectID('602fbe6eceddfb124692ae39'),
    info: [
      {
        jobseeker: new ObjectID('6058e4584a7c03dc824b054f'),
        status: 'wait'
      }
    ]
  }
]
