const ObjectID = require('mongodb').ObjectID
module.exports = [
  {
    _id: new ObjectID('602fbfda93f51e92200b6ee9'),
    name: 'Website Developer',
    skills: [
      new ObjectID('602fc035ff68454377e1ceff'),
      new ObjectID('602fc038d37c2f91865c6d95')
    ]
  },
  {
    _id: new ObjectID('6033b7db5b062490c308944e'),
    name: 'Programmer',
    skills: [
      new ObjectID('602fc035ff68454377e1ceff'),
      new ObjectID('602fc038d37c2f91865c6d95')
    ]
  }
]
