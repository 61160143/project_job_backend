const ObjectID = require('mongodb').ObjectID
module.exports = [
  {
    _id: new ObjectID('602fe36a92e124475c0036a8'),
    position: new ObjectID('602fbfda93f51e92200b6ee9'),
    company: new ObjectID('602fbe6eceddfb124692ae39'),
    recruitment: 5,
    startDate: '2021/02/02',
    endDate: '2021/03/03',
    province: 'ชลบุรี',
    salary: 30000,
    description: 'Poppy dlaks;dlksaoeiwqpoe\nาำนไๆานาวกสฟหานาๆไำนรl;kdasl;kdasopeiwqp\ndas;daskd;lkas;ldk',
    createDate: '2021/02/22'
  },
  {
    _id: new ObjectID('6033b7fac81f38f17643e256'),
    position: new ObjectID('6033b7db5b062490c308944e'),
    company: new ObjectID('602fbe6eceddfb124692ae39'),
    recruitment: 10,
    startDate: '2021/02/17',
    endDate: '2021/03/21',
    province: 'ชลบุรี',
    salary: 20000,
    description: 'Poppy dlaks;dlksaoeiwqpoeil;kdasl;kdasopeiwqp\ndas;daskd;lkas;ldk',
    createDate: '2021/02/17'
  }
]
