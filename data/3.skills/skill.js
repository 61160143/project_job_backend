const ObjectID = require('mongodb').ObjectID
module.exports = [
  {
    _id: new ObjectID('602fc035ff68454377e1ceff'),
    name: 'Frontend Website Developer'
  },
  {
    _id: new ObjectID('602fc038d37c2f91865c6d95'),
    name: 'Backend Website Developer'
  }
]
