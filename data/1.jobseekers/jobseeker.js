const ObjectID = require('mongodb').ObjectID
module.exports = [
  {
    _id: new ObjectID('601eb76d1e72782ca8ad6178'),
    name: 'Charlotte Dunoas',
    email: 'charlotte.d@gmail.com',
    phoneNumber: '0685426283',
    birthdate: '17/1/2543',
    isApprove: false,
    resume: ''
  },
  {
    _id: new ObjectID('6058e4584a7c03dc824b054f'),
    name: 'Bruno Fernandes',
    email: 'fernandes@gmail.com',
    phoneNumber: '0899999999',
    birthdate: '17/1/2535',
    isApprove: false,
    resume: ''
  }
]
