const ObjectID = require('mongodb').ObjectID
module.exports = [
  {
    _id: new ObjectID('60577ee37598e969329c4678'),
    name: 'user'
  },
  {
    _id: new ObjectID('60577f3894d00ab321c77799'),
    name: 'company'
  },
  {
    _id: new ObjectID('60577f3bec1e8d174d484d90'),
    name: 'admin'
  }
]
