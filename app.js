const createError = require('http-errors')
const express = require('express')
const cors = require('cors')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')

const indexRouter = require('./routes/index')
const positionsRouter = require('./routes/positions')
const companysRouter = require('./routes/company')
const helloRouter = require('./routes/hello')
const productsRouter = require('./routes/products')
const applicantRouter = require('./routes/applicants')
const detailRouter = require('./routes/detail')
const jobPostingRouter = require('./routes/jobPosting')
const applicationRouter = require('./routes/applications')
const resumeRouter = require('./routes/resume')
const authRouter = require('./routes/auth')
const userRouter = require('./routes/user')

// eslint-disable-next-line no-unused-vars
const mongoose = require('mongoose')
const app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'jade')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())

app.use('/', indexRouter)
app.use('/positions', positionsRouter)
app.use('/hello', helloRouter)
app.use('/products', productsRouter)
app.use('/companys', companysRouter)
app.use('/jobseekers', applicantRouter)
app.use('/detail', detailRouter)
app.use('/jobpostings', jobPostingRouter)
app.use('/applications', applicationRouter)
app.use('/resume', resumeRouter)
app.use('/auth', authRouter)
app.use('/user', userRouter)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404))
})
// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

mongoose.connect('mongodb://localhost/jobdb', { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false})

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error'))
db.once('open', function () {
  console.log('============================\n==== Mongo Server Start ====\n========= Port 3000 ========\n============================')
})

module.exports = app
