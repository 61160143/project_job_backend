const express = require('express')
const router = express.Router()
const jobPostingsController = require('../controller/JobPostingController')
const { verifySignUp, authJwt } = require("../app/middleware");

router.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });
/* GET users listing. */
router.get('/', jobPostingsController.getJobPostings)

router.get('/:id/company', jobPostingsController.getJobPostingByCompany)

router.get('/:id', jobPostingsController.getJobPosting)

router.post('/search', jobPostingsController.getSearchJobPostings)

router.post('/', jobPostingsController.addJobPosting)

router.put('/', jobPostingsController.updateJobPosting)

router.delete('/:id', jobPostingsController.delJobPosting)

module.exports = router
