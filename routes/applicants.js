const express = require('express')
const router = express.Router()
const jobSeekersController = require('../controller/JobSeekersController')

/* GET users listing. */
router.get('/', jobSeekersController.getJobSeekers)

router.get('/:id', jobSeekersController.getJobSeeker)

router.post('/', jobSeekersController.addJobSeeker)

router.put('/', jobSeekersController.updateJobSeeker)

router.delete('/:id', jobSeekersController.deleteJobSeeker)

router.put('/resume', jobSeekersController.updateResume)

module.exports = router
