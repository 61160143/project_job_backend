const { verifySignUp, authJwt } = require("../app/middleware");
const controller = require("../controller/UserController");
const express = require('express')
const router = express.Router()

router.use(function(req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});

router.get("/all",
  controller.allAccess
)

router.get('/user',
  [authJwt.verifyToken],
  controller.userBoard
)

router.get('/company',
  [authJwt.verifyToken, authJwt.isModerator],
  controller.moderatorBoard
)

router.get('/admin',
  [authJwt.verifyToken, authJwt.isAdmin],
  controller.adminBoard
)

module.exports = router