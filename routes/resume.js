const express = require('express')
const router = express.Router()
const resumeController = require('../controller/ResumeController')

/* GET users listing. */

router.get('/:id', resumeController.getResume)

module.exports = router
