const express = require('express')
const router = express.Router()

const products = [
  {
    id: 1001,
    name: 'Node.js for Beginners',
    category: 'Node',
    price: 990
  },
  {
    id: 1002,
    name: 'React 101',
    category: 'React',
    price: 3990
  },
  {
    id: 1003,
    name: 'Getting started with MongoDB',
    category: 'MongoDB',
    price: 1990
  }
]

router.get('/', (req, res, next) => {
  res.json(products)
})

router.get('/:id', (req, res, next) => {
  console.log(req.params)
  const { id } = req.params
  const result = products.find(product => product.id === id)
  res.json(result)
})

router.post('/', (req, res, next) => {
  const payload = req.body
  products.push(payload)
  res.json(payload)
})

router.put('/', (req, res, next) => {
  const payload = req.body
  const index = products.findIndex(product => product.id === payload.id)
  products.splice(index, 1, payload)
  res.json(payload)
})

router.delete('/:id', (req, res, next) => {
  console.log(req.params)
  const { id } = req.params
  const index = products.find(product => product.id === id)
  products.splice(index, 0)
  res.json({ id })
})

module.exports = router
