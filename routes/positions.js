const express = require('express')
const positionController = require('../controller/PositionController')
// const User = require('../models/User')
const router = express.Router()

/* GET users listing. */
router.get('/', positionController.getPositions)

router.get('/:id', positionController.getPosition)

router.post('/', positionController.addPosition)

router.put('/', positionController.updatePosition)

router.delete('/:id', positionController.deletePosition)

module.exports = router
