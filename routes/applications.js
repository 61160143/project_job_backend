const express = require('express')
const router = express.Router()
const applicationsController = require('../controller/ApplicationController')

/* GET users listing. */
router.get('/:id/company', applicationsController.getApplication)

router.post('/regapplication', applicationsController.registerApplication)

module.exports = router
