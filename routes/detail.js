const express = require('express')
const router = express.Router()
const detailController = require('../controller/DetailController')

router.get('/:id', detailController.getDetail)

module.exports = router
