const express = require('express')
const router = express.Router()

router.get('/', function (req, res, next) {
  res.status(200).send({
    code: '200',
    status: 'success',
    msg: '',
    time: '2021-02-22 09:16:21',
    param_obj: {
      prov_name: false,
      prov_code: false,
      prov_id: false,
      prov_gf_code: false
    },
    summary: { total: 77 },
    result: [
      {
        prov_code: '04810000',
        prov_name: '\u0e01\u0e23\u0e30\u0e1a\u0e35\u0e48',
        prov_id: '81',
        prov_gf_code: '8100'
      },
      {
        prov_code: '03100000',
        prov_name:
          '\u0e01\u0e23\u0e38\u0e07\u0e40\u0e17\u0e1e\u0e21\u0e2b\u0e32\u0e19\u0e04\u0e23',
        prov_id: '10',
        prov_gf_code: null
      },
      {
        prov_code: '03710000',
        prov_name: '\u0e01\u0e32\u0e0d\u0e08\u0e19\u0e1a\u0e38\u0e23\u0e35',
        prov_id: '71',
        prov_gf_code: '7100'
      },
      {
        prov_code: '02460000',
        prov_name: '\u0e01\u0e32\u0e2c\u0e2a\u0e34\u0e19\u0e18\u0e38\u0e4c',
        prov_id: '46',
        prov_gf_code: '4600'
      },
      {
        prov_code: '01620000',
        prov_name: '\u0e01\u0e33\u0e41\u0e1e\u0e07\u0e40\u0e1e\u0e0a\u0e23',
        prov_id: '62',
        prov_gf_code: '6200'
      },
      {
        prov_code: '02400000',
        prov_name: '\u0e02\u0e2d\u0e19\u0e41\u0e01\u0e48\u0e19',
        prov_id: '40',
        prov_gf_code: '4000'
      },
      {
        prov_code: '03220000',
        prov_name: '\u0e08\u0e31\u0e19\u0e17\u0e1a\u0e38\u0e23\u0e35',
        prov_id: '22',
        prov_gf_code: '2200'
      },
      {
        prov_code: '03240000',
        prov_name:
          '\u0e09\u0e30\u0e40\u0e0a\u0e34\u0e07\u0e40\u0e17\u0e23\u0e32',
        prov_id: '24',
        prov_gf_code: '2400'
      },
      {
        prov_code: '03200000',
        prov_name: '\u0e0a\u0e25\u0e1a\u0e38\u0e23\u0e35',
        prov_id: '20',
        prov_gf_code: '2000'
      },
      {
        prov_code: '03180000',
        prov_name: '\u0e0a\u0e31\u0e22\u0e19\u0e32\u0e17',
        prov_id: '18',
        prov_gf_code: '1800'
      },
      {
        prov_code: '02360000',
        prov_name: '\u0e0a\u0e31\u0e22\u0e20\u0e39\u0e21\u0e34',
        prov_id: '36',
        prov_gf_code: '3600'
      },
      {
        prov_code: '04860000',
        prov_name: '\u0e0a\u0e38\u0e21\u0e1e\u0e23',
        prov_id: '86',
        prov_gf_code: '8600'
      },
      {
        prov_code: '01570000',
        prov_name: '\u0e40\u0e0a\u0e35\u0e22\u0e07\u0e23\u0e32\u0e22',
        prov_id: '57',
        prov_gf_code: '5700'
      },
      {
        prov_code: '01500000',
        prov_name: '\u0e40\u0e0a\u0e35\u0e22\u0e07\u0e43\u0e2b\u0e21\u0e48',
        prov_id: '50',
        prov_gf_code: '5000'
      },
      {
        prov_code: '04920000',
        prov_name: '\u0e15\u0e23\u0e31\u0e07',
        prov_id: '92',
        prov_gf_code: '9200'
      },
      {
        prov_code: '03230000',
        prov_name: '\u0e15\u0e23\u0e32\u0e14',
        prov_id: '23',
        prov_gf_code: '2300'
      },
      {
        prov_code: '01630000',
        prov_name: '\u0e15\u0e32\u0e01',
        prov_id: '63',
        prov_gf_code: '6300'
      },
      {
        prov_code: '03260000',
        prov_name: '\u0e19\u0e04\u0e23\u0e19\u0e32\u0e22\u0e01',
        prov_id: '26',
        prov_gf_code: '2600'
      },
      {
        prov_code: '03730000',
        prov_name: '\u0e19\u0e04\u0e23\u0e1b\u0e10\u0e21',
        prov_id: '73',
        prov_gf_code: '7300'
      },
      {
        prov_code: '02480000',
        prov_name: '\u0e19\u0e04\u0e23\u0e1e\u0e19\u0e21',
        prov_id: '48',
        prov_gf_code: '4800'
      },
      {
        prov_code: '02300000',
        prov_name:
          '\u0e19\u0e04\u0e23\u0e23\u0e32\u0e0a\u0e2a\u0e35\u0e21\u0e32',
        prov_id: '30',
        prov_gf_code: '3000'
      },
      {
        prov_code: '04800000',
        prov_name:
          '\u0e19\u0e04\u0e23\u0e28\u0e23\u0e35\u0e18\u0e23\u0e23\u0e21\u0e23\u0e32\u0e0a',
        prov_id: '80',
        prov_gf_code: '8000'
      },
      {
        prov_code: '01600000',
        prov_name: '\u0e19\u0e04\u0e23\u0e2a\u0e27\u0e23\u0e23\u0e04\u0e4c',
        prov_id: '60',
        prov_gf_code: '6000'
      },
      {
        prov_code: '03120000',
        prov_name: '\u0e19\u0e19\u0e17\u0e1a\u0e38\u0e23\u0e35',
        prov_id: '12',
        prov_gf_code: '1200'
      },
      {
        prov_code: '04960000',
        prov_name: '\u0e19\u0e23\u0e32\u0e18\u0e34\u0e27\u0e32\u0e2a',
        prov_id: '96',
        prov_gf_code: '9600'
      },
      {
        prov_code: '01550000',
        prov_name: '\u0e19\u0e48\u0e32\u0e19',
        prov_id: '55',
        prov_gf_code: '5500'
      },
      {
        prov_code: '02500000',
        prov_name: '\u0e1a\u0e36\u0e07\u0e01\u0e32\u0e2c',
        prov_id: '38',
        prov_gf_code: '3800'
      },
      {
        prov_code: '02310000',
        prov_name: '\u0e1a\u0e38\u0e23\u0e35\u0e23\u0e31\u0e21\u0e22\u0e4c',
        prov_id: '31',
        prov_gf_code: '3100'
      },
      {
        prov_code: '03130000',
        prov_name: '\u0e1b\u0e17\u0e38\u0e21\u0e18\u0e32\u0e19\u0e35',
        prov_id: '13',
        prov_gf_code: '1300'
      },
      {
        prov_code: '03770000',
        prov_name:
          '\u0e1b\u0e23\u0e30\u0e08\u0e27\u0e1a\u0e04\u0e35\u0e23\u0e35\u0e02\u0e31\u0e19\u0e18\u0e4c',
        prov_id: '77',
        prov_gf_code: '7700'
      },
      {
        prov_code: '03250000',
        prov_name:
          '\u0e1b\u0e23\u0e32\u0e08\u0e35\u0e19\u0e1a\u0e38\u0e23\u0e35',
        prov_id: '25',
        prov_gf_code: '2500'
      },
      {
        prov_code: '04940000',
        prov_name: '\u0e1b\u0e31\u0e15\u0e15\u0e32\u0e19\u0e35',
        prov_id: '94',
        prov_gf_code: '9400'
      },
      {
        prov_code: '03140000',
        prov_name:
          '\u0e1e\u0e23\u0e30\u0e19\u0e04\u0e23\u0e28\u0e23\u0e35\u0e2d\u0e22\u0e38\u0e18\u0e22\u0e32',
        prov_id: '14',
        prov_gf_code: '1400'
      },
      {
        prov_code: '01560000',
        prov_name: '\u0e1e\u0e30\u0e40\u0e22\u0e32',
        prov_id: '56',
        prov_gf_code: '5600'
      },
      {
        prov_code: '04820000',
        prov_name: '\u0e1e\u0e31\u0e07\u0e07\u0e32',
        prov_id: '82',
        prov_gf_code: '8200'
      },
      {
        prov_code: '04930000',
        prov_name: '\u0e1e\u0e31\u0e17\u0e25\u0e38\u0e07',
        prov_id: '93',
        prov_gf_code: '9300'
      },
      {
        prov_code: '01660000',
        prov_name: '\u0e1e\u0e34\u0e08\u0e34\u0e15\u0e23',
        prov_id: '66',
        prov_gf_code: '6600'
      },
      {
        prov_code: '01650000',
        prov_name: '\u0e1e\u0e34\u0e29\u0e13\u0e38\u0e42\u0e25\u0e01',
        prov_id: '65',
        prov_gf_code: '6500'
      },
      {
        prov_code: '03760000',
        prov_name: '\u0e40\u0e1e\u0e0a\u0e23\u0e1a\u0e38\u0e23\u0e35',
        prov_id: '76',
        prov_gf_code: '7600'
      },
      {
        prov_code: '01670000',
        prov_name: '\u0e40\u0e1e\u0e0a\u0e23\u0e1a\u0e39\u0e23\u0e13\u0e4c',
        prov_id: '67',
        prov_gf_code: '6700'
      },
      {
        prov_code: '01540000',
        prov_name: '\u0e41\u0e1e\u0e23\u0e48',
        prov_id: '54',
        prov_gf_code: '5400'
      },
      {
        prov_code: '04830000',
        prov_name: '\u0e20\u0e39\u0e40\u0e01\u0e47\u0e15',
        prov_id: '83',
        prov_gf_code: '8300'
      },
      {
        prov_code: '02440000',
        prov_name: '\u0e21\u0e2b\u0e32\u0e2a\u0e32\u0e23\u0e04\u0e32\u0e21',
        prov_id: '44',
        prov_gf_code: '4400'
      },
      {
        prov_code: '02490000',
        prov_name: '\u0e21\u0e38\u0e01\u0e14\u0e32\u0e2b\u0e32\u0e23',
        prov_id: '49',
        prov_gf_code: '4900'
      },
      {
        prov_code: '01580000',
        prov_name:
          '\u0e41\u0e21\u0e48\u0e2e\u0e48\u0e2d\u0e07\u0e2a\u0e2d\u0e19',
        prov_id: '58',
        prov_gf_code: '5800'
      },
      {
        prov_code: '02350000',
        prov_name: '\u0e22\u0e42\u0e2a\u0e18\u0e23',
        prov_id: '35',
        prov_gf_code: '3500'
      },
      {
        prov_code: '04950000',
        prov_name: '\u0e22\u0e30\u0e25\u0e32',
        prov_id: '95',
        prov_gf_code: '9500'
      },
      {
        prov_code: '02450000',
        prov_name: '\u0e23\u0e49\u0e2d\u0e22\u0e40\u0e2d\u0e47\u0e14',
        prov_id: '45',
        prov_gf_code: '4500'
      },
      {
        prov_code: '04850000',
        prov_name: '\u0e23\u0e30\u0e19\u0e2d\u0e07',
        prov_id: '85',
        prov_gf_code: '8500'
      },
      {
        prov_code: '03210000',
        prov_name: '\u0e23\u0e30\u0e22\u0e2d\u0e07',
        prov_id: '21',
        prov_gf_code: '2100'
      },
      {
        prov_code: '03700000',
        prov_name: '\u0e23\u0e32\u0e0a\u0e1a\u0e38\u0e23\u0e35',
        prov_id: '70',
        prov_gf_code: '7000'
      },
      {
        prov_code: '03160000',
        prov_name: '\u0e25\u0e1e\u0e1a\u0e38\u0e23\u0e35',
        prov_id: '16',
        prov_gf_code: '1600'
      },
      {
        prov_code: '01520000',
        prov_name: '\u0e25\u0e33\u0e1b\u0e32\u0e07',
        prov_id: '52',
        prov_gf_code: '5200'
      },
      {
        prov_code: '01510000',
        prov_name: '\u0e25\u0e33\u0e1e\u0e39\u0e19',
        prov_id: '51',
        prov_gf_code: '5100'
      },
      {
        prov_code: '02420000',
        prov_name: '\u0e40\u0e25\u0e22',
        prov_id: '42',
        prov_gf_code: '4200'
      },
      {
        prov_code: '02330000',
        prov_name: '\u0e28\u0e23\u0e35\u0e2a\u0e30\u0e40\u0e01\u0e29',
        prov_id: '33',
        prov_gf_code: '3300'
      },
      {
        prov_code: '02470000',
        prov_name: '\u0e2a\u0e01\u0e25\u0e19\u0e04\u0e23',
        prov_id: '47',
        prov_gf_code: '4700'
      },
      {
        prov_code: '04900000',
        prov_name: '\u0e2a\u0e07\u0e02\u0e25\u0e32',
        prov_id: '90',
        prov_gf_code: '9000'
      },
      {
        prov_code: '04910000',
        prov_name: '\u0e2a\u0e15\u0e39\u0e25',
        prov_id: '91',
        prov_gf_code: '9100'
      },
      {
        prov_code: '03110000',
        prov_name:
          '\u0e2a\u0e21\u0e38\u0e17\u0e23\u0e1b\u0e23\u0e32\u0e01\u0e32\u0e23',
        prov_id: '11',
        prov_gf_code: '1100'
      },
      {
        prov_code: '03750000',
        prov_name:
          '\u0e2a\u0e21\u0e38\u0e17\u0e23\u0e2a\u0e07\u0e04\u0e23\u0e32\u0e21',
        prov_id: '75',
        prov_gf_code: '7500'
      },
      {
        prov_code: '03740000',
        prov_name: '\u0e2a\u0e21\u0e38\u0e17\u0e23\u0e2a\u0e32\u0e04\u0e23',
        prov_id: '74',
        prov_gf_code: '7400'
      },
      {
        prov_code: '03270000',
        prov_name: '\u0e2a\u0e23\u0e30\u0e41\u0e01\u0e49\u0e27',
        prov_id: '27',
        prov_gf_code: '2700'
      },
      {
        prov_code: '03190000',
        prov_name: '\u0e2a\u0e23\u0e30\u0e1a\u0e38\u0e23\u0e35',
        prov_id: '19',
        prov_gf_code: '1900'
      },
      {
        prov_code: '03170000',
        prov_name: '\u0e2a\u0e34\u0e07\u0e2b\u0e4c\u0e1a\u0e38\u0e23\u0e35',
        prov_id: '17',
        prov_gf_code: '1700'
      },
      {
        prov_code: '01640000',
        prov_name: '\u0e2a\u0e38\u0e42\u0e02\u0e17\u0e31\u0e22',
        prov_id: '64',
        prov_gf_code: '6400'
      },
      {
        prov_code: '03720000',
        prov_name:
          '\u0e2a\u0e38\u0e1e\u0e23\u0e23\u0e13\u0e1a\u0e38\u0e23\u0e35',
        prov_id: '72',
        prov_gf_code: '7200'
      },
      {
        prov_code: '04840000',
        prov_name:
          '\u0e2a\u0e38\u0e23\u0e32\u0e29\u0e0e\u0e23\u0e4c\u0e18\u0e32\u0e19\u0e35',
        prov_id: '84',
        prov_gf_code: '8400'
      },
      {
        prov_code: '02320000',
        prov_name: '\u0e2a\u0e38\u0e23\u0e34\u0e19\u0e17\u0e23\u0e4c',
        prov_id: '32',
        prov_gf_code: '3200'
      },
      {
        prov_code: '02430000',
        prov_name: '\u0e2b\u0e19\u0e2d\u0e07\u0e04\u0e32\u0e22',
        prov_id: '43',
        prov_gf_code: '4300'
      },
      {
        prov_code: '02390000',
        prov_name:
          '\u0e2b\u0e19\u0e2d\u0e07\u0e1a\u0e31\u0e27\u0e25\u0e33\u0e20\u0e39',
        prov_id: '39',
        prov_gf_code: '3900'
      },
      {
        prov_code: '03150000',
        prov_name: '\u0e2d\u0e48\u0e32\u0e07\u0e17\u0e2d\u0e07',
        prov_id: '15',
        prov_gf_code: '1500'
      },
      {
        prov_code: '02370000',
        prov_name:
          '\u0e2d\u0e33\u0e19\u0e32\u0e08\u0e40\u0e08\u0e23\u0e34\u0e0d',
        prov_id: '37',
        prov_gf_code: '3700'
      },
      {
        prov_code: '02410000',
        prov_name: '\u0e2d\u0e38\u0e14\u0e23\u0e18\u0e32\u0e19\u0e35',
        prov_id: '41',
        prov_gf_code: '4100'
      },
      {
        prov_code: '01530000',
        prov_name: '\u0e2d\u0e38\u0e15\u0e23\u0e14\u0e34\u0e15\u0e16\u0e4c',
        prov_id: '53',
        prov_gf_code: '5300'
      },
      {
        prov_code: '03610000',
        prov_name: '\u0e2d\u0e38\u0e17\u0e31\u0e22\u0e18\u0e32\u0e19\u0e35',
        prov_id: '61',
        prov_gf_code: '6100'
      },
      {
        prov_code: '02340000',
        prov_name:
          '\u0e2d\u0e38\u0e1a\u0e25\u0e23\u0e32\u0e0a\u0e18\u0e32\u0e19\u0e35',
        prov_id: '34',
        prov_gf_code: '3400'
      }
    ]
  })
})

module.exports = router
