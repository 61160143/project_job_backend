const mongoose = require('mongoose')
// const User = require('./models/User')
// const Com = require('./models/Company')
const App = require('./models/JobSeeker')

mongoose.connect('mongodb://admin1:admin@localhost/mydb', { useNewUrlParser: true, useUnifiedTopology: true })

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error'))
db.once('open', function () {
  console.log('connect')
})

// eslint-disable-next-line array-callback-return
// User.find(function (err, users) {
//   if (err) return console.error(err)
//   console.log(users)
// })
// eslint-disable-next-line array-callback-return
App.find(function (err, jobSeeker) {
  if (err) return console.error(err)
  console.log(jobSeeker)
})
